# clone
```sh
git clone git@gitlab.com:nicoandresr/my-awesome-conky.git ~/.config/conky
```
# Config
change [user] in paths
and connect a usb to create the user `media` path

# Convert old syntax
to convert old conky syntax is necessary to use the convert.lua script, first chmoding the file with
`chmod +x convert.lua` after that execute `./convert.lua old_script new_script`

# Run
run the conky from console with:

```bash
conky -c ~/.config/conky/conkyrc_seamod.lua &&
conky -c ~/.config/conky/qlocktwo.lua &&
conky -c ~/.config/conky/sunbird.lua &&
conky -c ~/.config/conky/archey3.lua
```

# Dependencies
Install `conky-lua` from Aur, 

- git clone conky-lua
- cd conky-lua
- makepkg -si
