conky.config = {
  background=true,
  use_xft=true,
  font="DejaVu Sans Mono:pixelsize=18",
  xftalpha=0.9,
  update_interval=30,
  total_run_times=0,
  own_window=true,
  own_window_transparent=true,
  own_window_type="override",
  own_window_hints="undecorated,below,sticky,skip_taskbar,skip_pager",
  double_buffer=true,
  draw_shades=false,
  draw_outline=false,
  draw_borders=false,
  draw_graph_borders=false,
  stippled_borders=0,
  border_width=0,
  alignment="top_left",
  gap_x=10,
  gap_y=80,
  no_buffers=true,
  color0="33AADD",
}
conky.text = [[
               +                OS: ${sysname}
               \#                Hostname: ${nodename}
              \#\#\#               Kernel Release: ${kernel}
             \#\#\#\#\#              Time: ${time %k:%M}
             \#\#\#\#\#\#             NodeJs: ${exec node -v}
            ; \#\#\#\#\#;            NPM: ${exec npm -v}
           +\#\#.\#\#\#\#\#            Processes: ${processes}
          +\#\#\#\#\#\#\#\#\#\#           RAM:  ${mem} / ${memmax}
         \#\#\#\#\#\#${color0}\#\#\#\#\#\#\#;         CPU: $cpu%
        \#\#\#\#\#\#\#\#\#\#\#\#\#\#\#+        TypeScript: ${exec tsc -v}
       \#\#\#\#\#\#\#   \#\#\#\#\#\#\#        Root: ${fs_used /} / ${fs_size /} (${fs_used_perc /}%)
     .\#\#\#\#\#\#;     ;\#\#\#;`".      ${exec acpi -b}
    .\#\#\#\#\#\#\#;     ;\#\#\#\#\#.
    \#\#\#\#\#\#\#\#\#.   .\#\#\#\#\#\#\#\#`
   \#\#\#\#\#\#'           '\#\#\#\#\#\#
  ;\#\#\#\#                 \#\#\#\#;
  \#\#'                     '\#\#
 \#'                         `\#
]]
