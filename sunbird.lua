conky.config = {
	background = true,
	own_window = true,
	own_window_type = 'override',
	own_window_class = 'Conky',
	own_window_transparent = true,
	own_window_argb_visual = true,
	own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
	alignment = 'middle_right',
	gap_x = 60,
	gap_y = -200,
	use_xft = true,
	font = 'Source Sans Pro:size=18',
	xftalpha = 0.1,
	xinerama_head = 0,
	update_interval = 15.0,
	uppercase = true,
	double_buffer = true,
	own_window_argb_visual = true,
	own_window_argb_value = 50,
	draw_shades = true,
	default_shade_color = '#000000',
	default_color = '#2d2d2d',
	color1 = '#ffffff',
};

conky.text = [[
${alignc}${exec LANG=en_US date +"%a" -d "-1 days"} ${color1}${exec LANG=en_US date +"%a"}${color} ${exec LANG=en_US date +"%a" -d "+1 days"}
${alignc}${exec date +"%d" -d "-1 days"} ${color1}${exec date +"%d"}${color} ${exec date +"%d" -d "+1 days"}
]];
